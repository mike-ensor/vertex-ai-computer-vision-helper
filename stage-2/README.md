# Overview

Stage 2 will take the export of LabelStudio JSON-Min file with label information and convert it to Vertext AI format to be used by Vertex Pipelines.

Steps:
1. Run `setup-env.sh`
1. Label data in LabelStudio
1. Export json-min format
1. Rename file to match file convention
1. Move file to `sources/` folder in Stage 2
1. Run the conversion
1. Upload converted file to GCS bucket
1. Import new changes to the dataset
1. Kick off an iteration of the Vertex Pipeline (ie: retrain the model)

## Exporting JSON-Min from LabelStudio

1. Click on "Export" from the main project page (not the Admin, but where labeling is done).
1. Select "JSON-Min" and download
1. Rename the file with the following convention: `<dataset-name>-json-min-[vX.Y.Z].json`
1. Place the file in the local `./stage-2/source/` folder

    ```shell
    # Example
    export VERSION="0.0.1"
    mv ~/downloads/project-1-json-min.json ./stage-2/source/${BASE_SOURCE_FILE_NAME}-json-min-v${VERSION}.json
    ```

1. Setup an `.envrc` file (and `source .envrc` it) in the folder or set the ENV files below
1. Run the `./convert.py $VERSION` file (or `python convert.py $VERSION`)
1. Use the provided command to copy the convereted `jsonl` file the GCS bucket for import
1. Create or add to Vertex Dataset (see next section)

## Data Import Vertex AI Dataset

Existing labeled data can be imported into a VertexAI Dataset, but only through a `.jsonl` file in VertexAI format and residing in a GCS bucket. Previous steps have created this condition so import can happen.

1. Go to VertexAI -> Dataset
1. Create a new dataset if this is the first time with a brand new set of images.
    1. Give it a name and fill in the relevant data
1. With the new or existing dataset, click on "Import" tab at the top of the main page body
1. Select "From GCS bucket" (last bullet point)
1. Navigate the file selector to the GCS bucket (ie: `gs://<bucketname>/training/json-import`) and select the version of the `.jsonl` to import.

    ![import-from-gcs](../docs/import-from-gcs.png)

1. Import can take a minute or two, or several hours depending on the quantity of the images
1. If prompted, choose "Default" for the Data Split (let Vertex and the Pipeline determine data split)

### Viewing Imported images
1. Click on "Browse" to see the imported images after completion

    1. It is OK if the file says some items were not imported, check to see how many labels were recognized

1. Begin training the model according to [Model Training](../docs/model-training.md)
1. Once the model pipeline has completed, move on to Stage 3


## Developer: Run Tests

```shell
python -m unittest tests.test_conversion -v
```