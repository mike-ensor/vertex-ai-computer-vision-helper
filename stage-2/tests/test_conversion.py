import unittest
from convert import ConvertToVertexAiJson


class ConvertToVertexTests(unittest.TestCase):
    def test_standard_percentage(self):
        self.assertEqual(ConvertToVertexAiJson.get_percent(50), 0.5)

    def test_whole_number_percentage(self):
        self.assertEqual(ConvertToVertexAiJson.get_percent(100), 1.0)

    def test_over_100_percent(self):
        self.assertEqual(ConvertToVertexAiJson.get_percent(150), 1.0)

    def test_zero_percent(self):
        self.assertEqual(ConvertToVertexAiJson.get_percent(0), 0.0)

    def test_negative_percentage(self):
        self.assertEqual(ConvertToVertexAiJson.get_percent(-25), 0.0)

    def test_decimal_percentage(self):
        self.assertAlmostEqual(ConvertToVertexAiJson.get_percent(33.3), 0.333,
                               places=3)  # Handle floating-point with places

    def test_large_negative_percentage(self):
        self.assertEqual(ConvertToVertexAiJson.get_percent(-1000), 0.0)

    def test_zero_division(self):
        with self.assertRaises(ZeroDivisionError):
            ConvertToVertexAiJson.get_percent(0.0) / 0


if __name__ == '__main__':
    unittest.main()
