#!/bin/bash

# Check if the .envrc file exists and if not, copy it from stage-0
if [[ ! -f ".envrc" ]]; then
    cp ../stage-0/.envrc .envrc
    source .envrc
    # Echo the only new addition to the .envrc file (if it doesn't already exist)
    if [[ -z "${BASE_SOURCE_FILE_NAME}" ]]; then
        echo '# This is the filename prefix of the exported dataset files' >>.envrc
        echo 'export BASE_SOURCE_FILE_NAME="<base-source-file-name>"' >>.envrc
    fi
fi

# Create a virtual environment
python3 -m venv .venv

# Activate the virtual environment
source .venv/bin/activate

# Update & Install dependencies
python3 -m pip install --upgrade pip
python3 -m pip --version
python3 -m pip install -r requirements.txt

source_folder="./source"
output_folder="./output"

echo "Creating folder to place exported LabelStudio files into: ${source_folder}"
mkdir -p "${source_folder}"
echo "Creating folder to place exported dataset files into: ${output_folder}"
mkdir -p "${output_folder}"

echo ""
echo "Copy the exported LabelStudio *.json-min.json file to ${source_folder}/<base_source_file_name>-json-min-<version>.json"
echo ""
echo "Verify the .envrc file is correct and run ./convert.sh <version>"
echo ""
