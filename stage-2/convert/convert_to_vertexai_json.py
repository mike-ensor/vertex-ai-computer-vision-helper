import random
from typing import Any, Hashable, Mapping

import yaml
import json
from openapi_schema_validator import validate

# Function to convert YAML to JSON
def yaml_to_json(yaml_file_path):
    with open(yaml_file_path, 'r') as file:
        # Load YAML data
        yaml_data: Mapping[Hashable, Any]  = yaml.safe_load(file)

    # Convert YAML data to JSON string
    # json_data = json.dumps(yaml_data, indent=4)
    return yaml_data

# Load this once. File pulled from https://cloud.google.com/vertex-ai/docs/image-data/object-detection/prepare-data#yaml-schema-file
SCHEMA = yaml_to_json("schema_dataset_ioformat_image_bounding_box_io_format_1.0.0.yaml")

class ConvertToVertexAiJson:
    # JSON Min to Vertex AI JSON
    def convert_annotations(
        self, input_file, output_file, annotation_set_name, ml_use="training"
    ) -> set:
        with open(input_file, "r") as infile, open(output_file, "w") as outfile:
            try:
                data = json.load(infile)
            except json.JSONDecodeError as e:
                print(f"Error decoding JSON: {e}")
                return set()

            if not isinstance(data, list):
                print(f"Error: Input file does not contain a list of objects.")
                return set()

            found_labels = set()
            converted_data = []
            for entry in data:
                bounding_boxes = []
                try:
                    image_gcs_uri = entry["image"]

                    # TODO: Has Annotation Data listed (only once)
                    has_annotation = False

                    for item_label in entry["label"]:
                        # Add to the collection of labels discovered (if there are more than 1, all will be added)
                        #     This is a rare case to have more than 1, would be the case that 2 overlapping
                        #     labels are present
                        label_name_list = item_label["rectanglelabels"]
                        all_labels = set(label_name_list)
                        found_labels |= all_labels
                        # Convert from
                        label_name = label_name_list[0]

                        x_min, y_min = item_label["x"], item_label["y"]
                        x_max, y_max = (
                            x_min + item_label["width"],
                            y_min + item_label["height"],
                        )
                        bounding_boxes.append(
                            {
                                "displayName": str(label_name),
                                "xMin": self.get_percent(x_min),
                                "yMin": self.get_percent(y_min),
                                "xMax": self.get_percent(x_max),
                                "yMax": self.get_percent(y_max),
                                "annotationResourceLabels": {
                                    "aiplatform.googleapis.com/annotation_set_name": annotation_set_name,
                                },
                            }
                        )

                    line = {
                            "imageGcsUri": image_gcs_uri,
                            "boundingBoxAnnotations": bounding_boxes,
                            # "dataItemResourceLabels": {
                            #     "aiplatform.googleapis.com/ml_use": self.get_ml_use()
                            # },
                        }

                    if not self.validate_schema(line):
                        print(f"Error validating line: {line}")
                    else:
                        converted_data.append(line)

                except KeyError as e:
                    continue  # print(f"Warning: Missing key in entry: {e}. Skipping...")

            json.dump(converted_data, outfile, indent=2)

            return found_labels

    # Set up an 75/10/5 tagging for training/test/validation
    def get_ml_use(self) -> str:
        rand_num = random.random()  # Generate a random float between 0.0 and 1.0

        if rand_num < 0.75:
            return "training"  # 20% of the time
        elif rand_num < 0.85:  # Equivalent to 75% + 10% = 95%
            return "test"  # 10% of the time
        else:
            return "validation"  # Remaining 5% of the time

    @staticmethod
    def get_percent(value) -> float:
        pct = value / 100.0
        if pct > 1.0:
            return 1.0
        elif pct < 0.0:
            return 0.0
        return pct

    @staticmethod
    def validate_schema(json_line) -> bool:
        # This could be a static or constant one-time load
        try:
            validate(json_line, SCHEMA)
        except Exception as e:
            print(f"Error validating line: {e}")
            return False

        return True

    @staticmethod
    def convert_to_jsonl(input_file, output_file):
        with open(input_file, "r") as infile, open(output_file, "w") as outfile:
            try:
                data = json.load(infile)
            except json.JSONDecodeError as e:
                print(f"Error decoding JSON: {e}")
                return

            if not isinstance(data, list):
                print("Error: Input file does not contain a list of objects.")
                return

            for obj in data:
                json.dump(obj, outfile)  # Dump each object as a separate line
                outfile.write("\n")  # Add newline after each object

# POST https://${REGION}-aiplatform.googleapis.com/v1/projects/${PROJECT_ID}/locations/${REGION}/datasets/${DATASET_ID}:import
# {
#   "importConfigs": [
#     {
#       "gcsSource": {
#         "uris": [
#           "gs://${BUCKET_NAME}/${OUTPUT_FILENAME}.jsonl"
#         ]
#       },
#       "import_schema_uri" : "gs://google-cloud-aiplatform/schema/dataset/ioformat/image_bounding_box_io_format_1.0.0.yaml"
#     }
#   ]
# }
