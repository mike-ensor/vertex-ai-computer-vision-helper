#!/usr/bin/env python

import os
import sys

from convert import ConvertToVertexAiJson

SOURCE_FOLDER = os.getenv("SOURCE_FOLDER", "source")
OUTPUT_FOLDER = os.getenv("OUTPUT_FOLDER", "output")
BASE_SOURCE_FILE_NAME = os.getenv("BASE_SOURCE_FILE_NAME")
ANNOTATION_SET_NAME = os.getenv("ANNOTATION_SET_NAME")

if __name__ == "__main__":
    arguments = sys.argv[1:]

    if len(arguments) != 1:
        print("Usage: convert.py <version>")
        exit(1)

    version = sys.argv[1]  # os.getenv("VERSION")
    bucket_name = os.getenv("DATASET_DESTINATION")

    print("====================================")
    print("Starting Conversion using version: {}".format(version))

    # Get the base folder for this script
    current_working_directory = os.getcwd()

    # TODO: Add check for if version does not exist
    if version is None:
        print("VERSION environment variable does not exist. Exiting")
        exit(1)

    annotation_set_name = ANNOTATION_SET_NAME

    # Establish file names for source (json-min exported file)
    input_filename = "{}-{}-{}.json".format(BASE_SOURCE_FILE_NAME, "json-min", version)
    input_full_file = "{}/{}/{}".format(
        current_working_directory, SOURCE_FOLDER, input_filename
    )
    # output folder & files (both JSON and JSONL)
    output_filename = "{}-{}-{}.json".format(BASE_SOURCE_FILE_NAME, "vertexai", version)
    output_full_file = "{}/{}/{}".format(
        current_working_directory, OUTPUT_FOLDER, output_filename
    )
    # Create a "labels" file
    labels_file = "labels-{}.txt".format(version)
    labels_full_file = "{}/{}/{}".format(
        current_working_directory, OUTPUT_FOLDER, labels_file
    )

    # Call Conversion
    convert = ConvertToVertexAiJson()

    discovered_labels = convert.convert_annotations(
        input_full_file, output_full_file, annotation_set_name, "training"
    )

    # Write the labels to a file
    with open(labels_full_file, "w") as labels_file:
        for label in discovered_labels:
            labels_file.write(label + "\n")

    # Convert to jsonl
    convert.convert_to_jsonl(output_full_file, output_full_file + "l")

    print("Conversion complete.")
    print("Labels discovered: {}".format(len(discovered_labels)))
    print("Output file: {}".format(output_full_file))
    print("Output file (jsonl): {}".format(output_full_file + "l"))
    print("Version: {}".format(version))
    print("====================================")
    print("Next Steps:")
    print("====================================")
    print(
        "1. Upload the JSONL file to GCS Bucket.\n\ngsutil cp {} {}/{}\n".format(
            output_full_file + "l", bucket_name, output_filename + "l"
        )
    )
    print("2. Import the JSONL file into dataset in Vertex AI. See docs for more info.")
    print("3. (Re)Train the model using Pipelines and Dataset combination.")
    print("4. Move on to Stage 3\n")
