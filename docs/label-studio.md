# Overview

As stated in the [README.md](../README.md), LabelStudio is a best-in-class tool used to label data. The key benefits of LabelStudio is the easy of use and user experience. LabelStudio allows you to focus on labeling your data (in this case, images) in an efficient method. Label Studio also provides multiple import and export file formats allowing the effort to be leveraged in VertexAI or other ML Ops.

## Install

LabelStudio has multiple methods to install, however the most simplistic is to [install on Kuberentes](https://labelstud.io/guide/install_k8s) using the Helm install. Recommend creating a LoadBalancer to easily expose the primary service. If GKE/K8s is not possible, running as a local Docker container would be a good method. Note, make sure you are using proper volume attachment to keep the stateful data produced by labeling process.

## Setup Label Studio project

The top-level structure in LabelStudio is a "Project". After logging in, create a new project and give it a name. From, there, the next step should be to create a label configuration structure, and then connect it to GCS buckets for efficiency.

> NOTE: This section should be performed AFTER `stage-0` environment has been set up. The values needed and bucket endpoints will not have been created until setup has occured.

1. Create a new project
1. Setup the "Labeling Interface"
   1. Select "Object Detection" for the type of project
1. Setup the labels

   1. Select "Code" in the top-middle tab ("Code" vs "Visual") section and modify the following to match your needs:

   ```xml
   <View>
       <Image name="image" value="$image" zoomControl="true" zoom="true"/>
       <RectangleLabels name="label" toName="image" choice="multiple">
           <!-- Change these values to match your labels. The background attribute is optional -->
           <Label value="red_hat" background="red"/>
           <Label value="white_hat" background="white"/>
           <Label value="blue_hat" background="blue"/>
           <Label value="green_hat" background="green"/>
       </RectangleLabels>
   </View>
   ```

## Setup Image Import

After the `stage-0` has set up the GCS bucket, the Cloud Storage can be setup/configured in LabelStudio.

1. Edit the Settings for the project (top-right corner)

1. Select "Cloud Storage"

    ### Source Cloud Storage

    1. Add source "Google Cloud Storage"
    1. Set the following

        | Field             | Description                                                   | Example Value |
        | ----------------- | ------------------------------------------------------------- | ------------- |
        | Storage Title     | The title you want to use for the connection                  | Source Images |
        | File Filter Regex | Regex to identify what files in the bucket should be selected | `.\*(jpe?g|png|gif|tiff)` |
        | Bucket Name | Name of the GCS bucket without `gs://` prefix | example-bucket-name |
        | Bucket Prefix | Folder path in the bucket where images exist. Note, no leading or training `/` | training/source/images |
        | Google Application Credentials | Full text value of the GSA JSON Key file | [emitted] |

    1. Test the connection and save.
    1. Click on "Sychronize"

    ### Target Cloud Storage

    1. Repeat the above section for the *Target Cloud Storage*

1. Click on "Project" then "Labeling".
1. Click on "Label all Tasks" to begin labeling
1. After some time, "Syncronize" the "Target Cloud Storage" to upload the LabelStudio metadata.
1. Adding any new images to the GCS bucket, then run "Synchronize" add the new images as Tasks to be labeled