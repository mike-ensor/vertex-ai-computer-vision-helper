#!/bin/bash

MODEL_FILE_NAME="model.tflite"
LABEL_FILE_NAME="labels.txt"
DICTIONARY_FILE_NAME="dict.txt"

# does the current directory have model.tflite
if [[ ! -f "${MODEL_FILE_NAME}" ]]; then
    echo "${MODEL_FILE_NAME} not found in current directory"
    exit 1
fi

# Check if dict.txt exists
if [[ -f "${DICTIONARY_FILE_NAME}" ]]; then
    echo "Existing ${DICTIONARY_FILE_NAME} found, removing..."
    rm -rf "${DICTIONARY_FILE_NAME}"
fi

# Extract one file the model.tflite using unzip looking for dict.txt
unzip "${MODEL_FILE_NAME}"

# Check if dict.txt exists
if [[ ! -f "${DICTIONARY_FILE_NAME}" ]]; then
    echo "${DICTIONARY_FILE_NAME} not found in model.tflite. Perhaps the model does not have the metatadata, failing..."
    exit 1
fi

echo "Renaming ${DICTIONARY_FILE_NAME} to ${LABEL_FILE_NAME}"

mv "${DICTIONARY_FILE_NAME}" "${LABEL_FILE_NAME}"

if [[ ! -f "${LABEL_FILE_NAME}" ]] || [[ ! -f "${MODEL_FILE_NAME}" ]]; then
    echo "${LABEL_FILE_NAME} or ${MODEL_FILE_NAME} directory after operation, failing..."
    exit 1
fi

# Specifically exit with status 0
exit 0