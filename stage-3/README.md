# Overview

This part of the project is intended to take the model generated from the Vertex Pipeline, extract the `labels.txt` and `model.tflite`, then push to the GCS bucket for the inference server to use. The model will be pushed to a semver based folder structure.

## Extracting Model

1. Copy the model URI from the last stage in the pipeline as depected below. Only copy the `gs://....`

    ![import-from-gcs](../docs/vertex-pipeline-complete.png)

1. Run the extraction script

    > NOTE: model_location needs a full URI path including 'gs://'

    ```shell
    export VERSION="0.0.1"
    ./place-model.sh "gs://...." ${VERSION}
    ```
1. Your model is now ready to be used. The extacted model will be found at the `${MODEL_DESTINATION_BUCKET}/${VERSION}/` directory

    ```shell
    gsutil ls -al ${MODEL_DESTINATION_BUCKET}/${ANNOTATION_SET_NAME}/${VERSION}/

         460 B    2024-11-31T12:34:56Z  gs://example-bucket/model-name/0.1.3/labels.txt
        3.55 MiB  2024-11-31T12:34:56Z  gs://example-bucket/model-name/0.1.3/model.tflite
        TOTAL: 2 objects, 3727050 bytes (3.55 MiB)
    ```

1. Done! Any inference server should be able to pull from the output bucket above. The bucket URIs above account for a Versioned .tflite model that can be used to inference.