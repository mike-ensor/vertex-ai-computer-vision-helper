#!/bin/bash

model_location=${1:-$TRAINING_EXPORT_MODEL_URI}
version=${2:-${MODEL_VERSION}}

model_name="${ANNOTATION_SET_NAME}"
destination_bucket=${MODEL_DESTINATION_BUCKET}

function usage() {
    echo "Usage: $0 [gcs-model-location] [version]"
    echo -e "\t- model_location: The URI to the pipeline exported model.tflite is located. ie: gs://gcs-bucket-uri"
    echo -e "\t- version: The new version of the model in vX.Y.Z format (semver)"
}

# Take in the full URL to the bucket and the model name produced by the training job
if [[ -z "${model_location}" ]]; then
    usage
    echo -e "\nERROR: The GCS URI to the model (model_location) is required. Exiting..."
    exit 1
fi

# Take in the full URL to the bucket and the model name produced by the training job
if [[ -z "${model_name}" ]]; then
    usage
    echo -e "\nERROR: The name of the model (ANNOTATION_SET_NAME) is required. Exiting..."
    exit 1
fi

# check if bucket exists
if [[ -z "${destination_bucket}" ]]; then
    usage
    echo -e "\nERROR: The destination bucket is required and not present. Exiting..."
    exit 1
fi

# check if bucket exists
if [[ "${destination_bucket}" != "gs://"* ]]; then
    usage
    echo -e "\nERROR: The destination bucket is not in format 'gs://<name>'. Exiting..."
    exit 1
fi

# check if bucket exists
if [[ -z "${version}" ]]; then
    usage
    echo -e "\nERROR: A version is required not present. Exiting..."
    exit 1
fi

# download the model.tflite from the full bucket endpoint
# check if model.tflite exists locally
if [[ -f "model.tflite" ]]; then
    echo "model.tflite already exists, skipping download."
else
    gsutil cp "${model_location}/model.tflite" .
fi

# Create the folder structure in the bucket using {bucket}/{model}/{version}
## NOTE: If bucket does NOT exist, the bucket will be created

# check if bucket exists using gsutil ls
if [[ $(gsutil ls -b "${destination_bucket}") ]]; then
    echo "Bucket exists, continuing..."
else
    echo "Bucket does not exist, creating..."
    gsutil mb "${destination_bucket}"
fi

# Proceed with placing the model in the bucket
echo "Version: ${version}"
echo "Model Name: ${model_name}"
echo "Destination Bucket: ${destination_bucket}"
echo "============================================="
echo "Are you sure you want to place the model.tflite in the bucket? (y/n)"
read -r response

case "$response" in
    [yY][eE][sS]|[yY]|[yY][aA])
        echo "Proceeding with placing the model in the bucket..."
        ;;
    *)
        echo "Operation cancelled."
        exit 1
        ;;
esac

# Extract the dict.txt from the modetflite
./tflite_dump.sh

if [[ $? -gt 0 ]]; then
    echo "Failed to extract labels from model.tflite"
    exit 1
fi

# Copy the model.tflite and labels.txt to the bucket
gsutil -m cp model.tflite "${destination_bucket}/${model_name}/${version}/model.tflite"
gsutil -m cp labels.txt "${destination_bucket}/${model_name}/${version}/labels.txt"

# List the contents of the bucket + version
gsutil ls "${destination_bucket}/${model_name}/${version}"

if [[ ! -z "${SKIP_CLEANUP}" ]]; then
    echo "Skipping cleanup..."
    exit 0
else
    echo "Cleaning up..."
    rm model.tflite labels.txt
fi
