# Overview

In Stage 1, the goal is to take the recorded videos in .mp4 format and convert them into a series of frames that can be used to label inside of LabelStudio.

:warning: This stage is dependent on running `../stage-0` setup.

## Process

1. If not already copied, copy the `.envrc` from `../stage-0` to `.envrc`
    * This should have happened if the `stage-0/validate-or-create-dataset.sh` was run properly
    * View/edit to ensure values are correct
1. Run `./slice.sh "<prefix>"` will pull the videos from the GCS bucket, slice the videos into frames, then push the images back into the bucket ready for Label Studio labeling.

    * Note the prefix of the specific videos to extract frames from. If the series of videos are named `image-group-a.mp4` and `image-group-b.mp4`, then the prefix could be `image-group`. This value NEEDS to be in quotes

1. Follow the prompts to automatically upload the frames to the Bucket under `${IMAGE_DESTINATION}` (usually set to `gs://<bucketname>/training/source/images`)

    > NOTE: There should be a comment "Extracted `n` frames. Proceed with uploading frames to bucket? (y/n)"

1. Proceed to [setup Label Studio](../docs/label-studio.md) or syncronize the LabelStudio Cloud Source Storage inside LabelStudio
1. Label the data in Label Studio according to the [Label Studio Docs](../docs/label-studio.md)
1. When done, proceed to `Stage 2`