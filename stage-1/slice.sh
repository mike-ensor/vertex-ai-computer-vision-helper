#!/bin/bash

# Check if video file is provided
if [[ $# -eq 0 ]]; then
    echo "Usage: $0 <video-file-prefix> [config-file]"
    echo "   - ConfigFile is optional"
    exit 1
fi

# Set the config file if not provided
if [[ -z "$2" ]]; then
    CONFIG_FILE=".envrc"
else
    CONFIG_FILE="$2"
fi

if [[ -f "${CONFIG_FILE}" ]]; then
    source ${CONFIG_FILE}
else
    echo "No .envrc fouor config-file not set or valid. Please run the stage-0 first."
    exit 1
fi

PREFIX=$1
FRAMES_PER_SECOND=${FRAMES_PER_SECOND:-0.75}
frame_dir="./frames"
video_temp="./temp-videos"

# Check if the video bucket exists
if [[ -z "${VIDEO_BUCKET}" ]]; then
    echo "VIDEO_BUCKET is not set in the config file. Exiting..."
    exit 1
fi

if [[ -z "${IMAGE_DESTINATION}" ]]; then
    echo "IMAGE_DESTINATION is not set in the config file. Exiting..."
    exit 1
fi

# Check if prefix is set
if [[ -z "${PREFIX}" ]]; then
    echo "Prefix is not set. Exiting..."
    exit 1
fi

echo "Attempting to pull videos from: ${VIDEO_BUCKET}/${PREFIX}*.mp4"

# images holds the array of links to images
images=($(gcloud storage ls "${VIDEO_BUCKET}/${PREFIX}*.mp4"))

mkdir -p "${video_temp}"
mkdir -p "${frame_dir}"

# For each image in the images array
for image in "${images[@]}"; do

    # Extract the filename with suffix (.mp4)
    base_name_with_suffix=$(basename "$image")
    echo "File With Suffix: $base_name_with_suffix"
    # Extract just the filename (no suffix)
    base_name="${base_name_with_suffix%.*}"
    echo "File Without Suffix: $base_name"

    if [[ -f "${video_temp}/$base_name_with_suffix" ]]; then
        echo "File already exists: ${video_temp}/${base_name_with_suffix}, Skipping download."
    else
        # Download the image
        echo "Downloading image: ${image}"
        gsutil cp "${image}" "${video_temp}"
    fi

    # Use ffmpeg to extract frames every 500ms
    ffmpeg -i "${video_temp}/$base_name_with_suffix" -vf "fps=${FRAMES_PER_SECOND}" "${frame_dir}/${base_name}-%04d.jpg"

    echo "Frames extracted to '$frame_dir'"
done

# count how many frames were extracted
frame_count=$(ls -1q "${frame_dir}" | wc -l)

echo "Extracted $frame_count frames"

# Proceede yes or no
read -p "Proceed with uploading frames to bucket? (y/n): " proceed

if [[ "$proceed" =~ ^[Yy](es)?$ ]]; then
    echo "Continuing the script..."
    # Copy all of the files in the frames directory to the bucket
    gcloud storage cp "${frame_dir}/*.jpg" "${IMAGE_DESTINATION}"
else
    echo "Aborting upload..."
    exit 0
fi

# Clean up
rm -rf "${video_temp}"
rm -rf "${frame_dir}"
