# Overview

In this first phase, the scripts will set up the GCS bucket with a folder convention. Project Setup including building a convention-based folder scheme in an existing or new GCS bucket.

## Steps
1. Change directory to the `stage-0` folder
1. Run the `./01-validate-or-crteate-new-dataset.sh [some-config-file-name.env]`

    This script will initially create an environment varaible configuration file based on the name provided.

1. Edit the configuration file generated to fill in the correct values for your environment
1. Re-run the `./01-validate-or-create-new-dataset.sh [the-config-file-name.env]` with the filled out configuration file

    The script will now validate the exsiting GCS bucket and the folder structure within. If items are not present, they will be created. This operation is idempotent.

    1. Produces the following folder structure if called with `./01-validate-or-create-new-dataset.sh build-a-thing.env`

        ![GCS Bucket Structure](../docs/folder-structure-dataset.png)

1. Record the videos of the items you want to classify. See [Video Recording Tips](#video-recording-tips).
1. Upload videos to the GCS bucket under ${VIDEO_BUCKET}


## Recording Videos

Recording videos can be performed using `ffmpeg`.  `ffmpeg` is sent an RTSP feed URL to record, so camera nees to be accessible by the machine performing the operation.

1. This project is assuming you have taken at least one video (.mp4 format) of your classifier(s) in use. The video should be 1+ minutes in length and taken at 30fps or greater. Multiple videos with multiple angles and real-world use of the classifier(s) is recommended.

    > NOTE: The resolution should be between 1024x567 and 1280x720 (recommended) and 16:9 ratio

1. Record your videos. Follow this convention when naming your videos: `<prefix>-<sequence-number>.mp4`. If you have an *RTSP* URL feed, use the following to caputre video from the feed: (See [#video-recording-tips](Video Recording Tips below))

    ```shell
    # URL to RTSP Feed (this is from an Axis camera, any camera with RTSP will work, but will have different URL schemes & user/password)
    export CAPTURE_URL="rtsp://myuser:mypass@192.168.1.2/axis-media/media.amp?videocodec=h265&camera=1"
    export PATH_TO_VIDEOS="~/training-videos"
    export VIDEO_PREFIX="initial-group" # example: ~/training-videos/misfits-1.mp4, ~/training-videos/misfits-2.mp4, ...

    # Record a stream
    ffmpeg -nostdin -rtsp_transport tcp -i "${CAPTURE_URL}" \
        -map 0 -c:v copy -c:a mp2 -y -f segment \
        -segment_time 300 -reset_timestamps 1 -strftime 1 \
        -loglevel verbose \
        "${PATH_TO_VIDEOS}/${VIDEO_PREFIX}-1.mp4  # <---- NOTE, this is the output file name

    # Hit "CTRL+C" to end recording
    ```
1. When complete upload the recordings of the classifier items in a .mp4 file(s) to the GCS bucket

    ```shell
    gsutil -m cp "${PATH_TO_VIDEOS}/${VIDEO_PREFIX}-*.mp4" "${VIDEO_BUCKET}"
    ```

### Video Recording Tips
* Avoid covering up the classifiers in the camera view (ie: poke or twist from the side)
* Place classifiers in all 4 corners
* Rotate the items 20 to 45 degrees, pause, then repeat
* Wait about 1/2 second before moving classifiers
* Use 2-4 classfiers at a time
  * Create little groups
  * If there are 2 of one type, record them at the same time and keep them opposite of each other
    * Try to avoid having the same dual classifier in the same state as the other one
* Use both a cool and warm light source
* Use multiple levels of light source
* Keep videos to 1 to 2 minutes max
* Use a prefix convention for the file names (ie: `gcafe-cupcakes-1.mp4`, `gcafe-cupcakes-2.mp4`, `gcafe-misfits-1.mp4`, etc)