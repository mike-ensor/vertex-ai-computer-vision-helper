#!/bin/bash

config=${1:-.envrc}
FOLDERS=("training/source/videos" "training/source/images" "training/json-import" "training/label-studio" "training/kubeflow" "training/feedback" "export/models")

function usage() {
  echo "./01-validate-or-create-new-dataset.sh <config-file>"
  echo "  * config-file: The file that contains the environment variables to use"
  exit 1
}

# Check if the CONFIG file exists
# If not, then the config file needs to created
# If it is, then the rest of the setup can happen
if [[ ! -f "${config}" ]]; then

  # Echo this into the .envrc / CONFIG file
  echo 'export PROJECT_ID="[gcp-project-id]"' >>"${config}"
  echo 'export FOLDER_BASE="training"' >>"${config}"
  echo 'export PRIMARY_GCS_BUCKET="[bucket-name]"' >>"${config}"

  echo '# Name of the annotation set' >>"${config}"
  echo 'export ANNOTATION_SET_NAME="my-annotation-set-name-change-me"' >>"${config}"

  echo '# Copy videos to this folder' >>"${config}"
  echo 'export VIDEO_BUCKET="gs://${PRIMARY_GCS_BUCKET}/${FOLDER_BASE}/source/videos"' >>"${config}"

  echo '# Sliced images will be uploaded here. Use this folder as the base for LabelStudio Cloud Bucket setup' >>"${config}"
  echo 'export IMAGE_DESTINATION="gs://${PRIMARY_GCS_BUCKET}/${FOLDER_BASE}/source/images"' >>"${config}"

  echo '# Location where jsonl dataset files will be pushed' >>"${config}"
  echo 'export DATASET_DESTINATION="gs://${PRIMARY_GCS_BUCKET}/${FOLDER_BASE}/json-import"' >>"${config}"

  echo '# Location where finished and exported models will be stored. This is where the Inference Server will be configured to look' >>"${config}"
  echo 'export MODEL_DESTINATION_BUCKET="gs://${PRIMARY_GCS_BUCKET}/export/models"' >>"${config}"

  echo '# GSK Key with authorization to GCS bucket. This is the same used in the Cloud Config Setup inside Label Studio' >> "${config}"
  echo 'export LABEL_STUDIO_GSA_KEY_FILE="./label-studio-gsa-key.json"' >>"${config}"
  echo 'export LABEL_STUDIO_GSA_EMAIL="<gsa-name>@<gcp-project>.iam.gserviceaccount.com"' >>"${config}"

  echo ""
  echo "Next: Edit .envrc and set variables before running, then run ./slice.sh"
  echo ""
  echo "Contents of ${config}:"
  echo "=============================="
  cat .envrc
  exit 0
else
  # Environment variables already exist, proceed with creating the environment/bucket setup
  echo "Setting up environment using '${config}' file"
  source "${config}"

  HAS_ERROR=0

  # Validate or Create the bucket
  BUCKET_EXISTS=$(gsutil ls -al "gs://${PRIMARY_GCS_BUCKET}" 2>&1 >/dev/null)

  if [[ $? -gt 0 ]]; then
    echo "GCS Bucket Does NOT exist or is not authenticated"
    echo "Creating GCS Bucket ${PRIMARY_GCS_BUCKET}"
    gsutil mb "gs://$PRIMARY_GCS_BUCKET"
  else
    echo "Bucket exists and current gcloud is authenticated: gs://${PRIMARY_GCS_BUCKET}"
  fi

  echo "Create/validate bucket folder structure"

  temp_file=$(mktemp)

  # create folder structure
  for folder in "${FOLDERS[@]}"; do

    # Check if folder exists, create or skip
    OPERATION=$(gsutil ls -al "gs://${PRIMARY_GCS_BUCKET}/${folder}" 2>&1 >/dev/null)
    if [[ $? -gt 0 ]]; then
      echo "[-] Folder '${folder}' does not exist, creating..."
      gsutil cp "${temp_file}" "gs://$PRIMARY_GCS_BUCKET/$folder/.keep"
    else
      echo "[X] ${folder} folder already exists"
    fi

  done

  rm "${temp_file}"

  # Add permission to the bucket using provided GSA email
  gsutil iam ch "serviceAccount:${LABEL_STUDIO_GSA_EMAIL}:objectAdmin" "gs://${PRIMARY_GCS_BUCKET}"

  echo '[
    {
        "origin": ["*"],
        "method": ["GET"],
        "responseHeader": ["Content-Type","Access-Control-Allow-Origin"],
        "maxAgeSeconds": 3600
    }
  ]' > cors-config.json

  # Set cors on the bucket
  gsutil cors set cors-config.json "gs://${PRIMARY_GCS_BUCKET}"

  # Remove the cors-config.json file
  rm cors-config.json

  echo "================================================================================================="
  echo ""
  echo "Record .mp4 videos according to the README.md and push them to the GCS bucket: ${VIDEO_BUCKET}"
  echo ""
  echo "When done, move on to Stage-1 to split up the videos into frames and upload them to the GCS bucket"
  echo ""
  echo "================================================================================================="
  echo ""
  echo "Copying the config file to ../stage-1
  cp "${config}" ../stage-1/${config}"
  echo ""

fi
