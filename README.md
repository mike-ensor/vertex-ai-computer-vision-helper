# Overview

This project is a collection of tools and processes to use both OSS labeling software and Google Cloud VertexAI products to train
computer vision based models.

:warning: NOTE & Disclosure: The author is a Googler and this is a side project. Please do your research, speak with your support team
and determine the path you would like to take for your business case.

> NOTE: [Read the blog post](https://medium.com/@mike-ensor/ai-edge-a-scalable-approach-d5878fe75d67) that covers the whole end-to-end process 

## End-to-End MLOps flow

![end-to-end flow](docs/end-to-end-ml-ops-diagram.png)

1. Collect data (images & video) in GCS bucket
2. Use VertexAI Datasets and Pipelines to train a model
3. Export model to a GCS bucket
4. Set the ConfigSync configuration for a location, pull the model to the edge
5. Inference/predict on the edge with your local applications
6. Collect regular feedback, push to a GCS bucket
7. Analyze data, create more labeled data
8. Loop

## Project Stages

The project is divided into 4 stages as outlined below.

```mermaid
stateDiagram-v2
    direction LR
    Stage0:Stage 0
    Stage1:Stage 1
    Stage2:Stage 2
    Stage3:Stage 3


    [*] --> Stage0

    Stage0 --> Stage1  : Bucket Setup
    note left of Stage0
        One-time setup per dataset
    end note

    Stage1 --> Stage2 : Upload Slice Images
    state Stage1 {
        record:Record videos
        upload_to_bucket:Upload to GCS
        slice.sh:Run Slice Script

        record --> upload_to_bucket
        upload_to_bucket --> slice.sh
    }

    Stage2 --> Stage3 : Convert And Upload Export File
    state Stage2 {
        label:Label Images
        export_json_min: Export JSON Min file
        convert.sh: Convert export to VertexAI
        upload_jsonl: Upload to GCS Bucket

        label --> export_json_min
        export_json_min --> convert.sh
        convert.sh --> upload_jsonl
    }

    Stage3 --> Stage1 : Repeat for new model version
    state Stage3 {
        train_model:Train Model with Vertex Pipelines
        export:Export to .tflite
        place_model.sh: Move versioned model

        train_model --> export
        export --> place_model.sh
    }

    Stage3 --> [*]

```

The end-to-end process of collecting, labeling, training and using a model can be broken up into a few high-level stages. Each of the stages below
can be done in a linear sequence that repeats.

### Stage 0 - [doc](stage-0/README.md)

Stage 0 has two primary purposes, to setup the GCP and local environment, and to upload MP4 videos of classifiers.

This stage assumes you have one or more videos of the items you would like to classify or perform inferencing on, and a running [LabelStudio](https://labelstud.io/) instance
where you have administation access to. For simplicity, recommend using Helm to install LabelStudio in a GKE instance.

### Stage 1 - [doc](stage-1/README.md)

Stage 1 is about converting a Video (`.mp4`) and turning it into frames. Frames are then uploaded to a GCS bucket so LabelStudio can index them. LabelStudio is then used to label the images for Stage 2.

### Stage 2 - [doc](stage-2/README.md)

This stage is responsible for taking an Export file from LabelStudio containing the labeled information against the GCS bucket images and converting that into
VertexAI autoML file format. VertexAI Dataset can only import pre-existing label information via GCS bucket and in jsonl format using the VertexAI standard.

### Stage 3 - [doc](stage-3/README.md)

This final stage takes the output of the trained model after running the Vertex Pipeline and having the .tflite model exported to a GCS bucket. Taking the model file
and putting it into a convention-based folder in the GCS bucket. The folder will indicate the version of the model, and the process will extract the labels.txt file
from the model.tflite for use by tflite inferencing.

## Why Label Studio?

VertexAI has a large set of tools to manage AI modeling, but is not highly intutitive and is lacking in the start of a model or data. Everything evolves around a "Dataset", and to create a dataset you have to either "upload" images through the UI (only up to 50), or you have to import with a file (ie: proprietary data format). In this case, the files to upload are going to be large quantity to start with, about 200 or more. It is possible to upload about 10, establish the secondary/new GCS bucket, then uplod the rest of the images to that bucket if it weren't for the second reason. The UX for labeling tasks in Google isn't great. When comparing to third-party tooling like LabelStudio, the features are very basic. One example is using keyboard shortcuts and auto-advance when labeing objects. Additionally, LabelStudio offers a "prediction API" to pre-suggest labeled items after a label has been established.

LabelStudio is "free", but "free like a puppy" as there are responsibilities you need to perform and little one-offs that need to be addressed to be an active functional tool in the AI Pipeline. If you find the project valueable enough to use, consider donating time or money to the project to help the team out.

## Docs

* [Kubeflow Pipeline Info](docs/kubeflow-pipeline.md)
* [Setup Label Studio](docs/label-studio.md)
* [Train a model](docs/model-training.md)